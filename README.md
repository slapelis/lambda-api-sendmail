# Lambda-API-Sendmail
A Terraform module to build infrastructure required for creating a simple serverless contact form.

### How to use
1. 
```terrraform
module "las" {
    source = "git::git@gitlab.com:slapelis/lambda-api-sendmail.git"
    role_name = "las_role"
    function_name = "lambda-function-name"
    billing_tag = "las"
    api_gateway_name = "las"
    api_gateway_description = "API gateway for las"
}
```
2. Copy the `exports.js` file from this repository and change the sender/receiver emails to whatever you want configured. Note that the sender email will need to be configured in Simple Email Service.
3. Zip `exports.js` to `exports.js.zip`.
3. Run the config.